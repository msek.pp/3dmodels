# 3DModels

All 3D models I've made until now. Parts created in Autodesk Inventor Professional 2023. 
Models are listed below and images are in corresponding directories.

## Minisumo parts

- Holder for PCB
- Holder for TOF Sensors (VL53L0X)
- Robot's front - sheet metal

## Automatic flower pot

- Small gear
- Big gear
- Holder for PCB
- Holder for servo
- Pot's separator

## Card drawer for MTG players

- Drawer
- Drawer's front
- Drawer's cover